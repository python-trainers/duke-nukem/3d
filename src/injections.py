from trainerbase.codeinjection import AllocatingCodeInjection
from trainerbase.memory import pm


infinite_health = AllocatingCodeInjection(
    pm.base_address + 0x74469,
    f"""
        mov byte [edx + {pm.base_address} + 0xC8683A], 200
    """,
    original_code_length=7,
)

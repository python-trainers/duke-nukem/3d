from contextlib import suppress

from pymem.exception import MemoryWriteError
from trainerbase.scriptengine import ScriptEngine

from objects import ammo_slots


script_engine = ScriptEngine(0.1)


@script_engine.simple_script
def infinite_ammo():
    for ammo in ammo_slots:
        with suppress(MemoryWriteError):
            ammo.value = 9999

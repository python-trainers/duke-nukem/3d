from trainerbase.gameobject import GameInt, GameShort

from memory import ammo_address, player_coords_address


ammo_slots = [GameShort(ammo_address + offset) for offset in range(0, GameShort.ctype_size * 10, GameShort.ctype_size)]

player_x = GameInt(player_coords_address)
player_y = GameInt(player_coords_address + 0x4)
player_z = GameInt(player_coords_address + 0x8)

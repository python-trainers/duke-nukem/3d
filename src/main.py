from trainerbase.main import run

from gui import run_menu
from scripts import script_engine


def on_initialized():
    script_engine.start()


def on_shutdown():
    script_engine.stop()


if __name__ == "__main__":
    run(run_menu, on_initialized, on_shutdown)

from trainerbase.gui.helpers import add_components, simple_trainerbase_menu
from trainerbase.gui.injections import CodeInjectionUI
from trainerbase.gui.misc import SeparatorUI
from trainerbase.gui.scripts import ScriptUI
from trainerbase.gui.speedhack import SpeedHackUI
from trainerbase.gui.teleport import TeleportUI

from injections import infinite_health
from scripts import infinite_ammo
from teleport import tp


@simple_trainerbase_menu("Duke Nukem 3D: 20th Anniversary World Tour", 630, 430)
def run_menu():
    add_components(
        CodeInjectionUI(infinite_health, "Infinite Health", "PageUp"),
        ScriptUI(infinite_ammo, "Infinite Ammo", "PageDown"),
        SeparatorUI(),
        TeleportUI(tp),
        SeparatorUI(),
        SpeedHackUI(default_factor_input_value=0.5),
    )
